#include <iostream>
#include <math.h>
#include <vector>


#include <fstream>
#include <string>
#include <iostream>

//int main() {
//    std::string input;
//    std::cin >> input;
//    std::ofstream out("output.txt");
//    out << input;
//    out.close();
//    return 0;
//}

using namespace std;

// Barrel Terrain math
class Point3D {
public:
  double X, Y, Z;
  Point3D() : Point3D(0, 0, 0) { }
  Point3D(double X0, double Y0, double Z0) {
    this->X=X0; this->Y=Y0; this->Z=Z0;
  }
  double Magnitude() {
    return sqrt( (this->X*this->X) + (this->Y*this->Y) + (this->Z*this->Z) );
  }
  double Distance(Point3D &other) {
    double DX, DY, DZ;
    DX = this->X-other.X; DY = this->Y-other.Y; DZ = this->Z-other.Z;
    return sqrt( (DX*DX) + (DY*DY) + (DZ*DZ) );
  }
};

class GridVec2 : vector<Point3D>  {
public:
  int Wdt, Hgt;//  uintmax_t
  GridVec2(int Wdt0, int Hgt0) : vector<Point3D>(Wdt0 * Hgt0) {
    this->Wdt = Wdt0; this->Hgt = Hgt0;
  }
  Point3D& Get(int XLoc, int YLoc) {
    int Dex = (YLoc * this->Wdt) + XLoc;
    return this->at(Dex);
  }
  void Set(int XLoc, int YLoc, Point3D Value) {
    int Dex = (YLoc * this->Wdt) + XLoc;
    this->at(Dex) = Value;
  }
  int GetDex(int XLoc, int YLoc) { return (YLoc * this->Wdt) + XLoc; }
};

class GridVec {
public:
  vector<Point3D> Ray;
  int Wdt, Hgt;//  uintmax_t
  GridVec(int Wdt0, int Hgt0) {
    this->Wdt = Wdt0; this->Hgt = Hgt0;
    this->Ray.resize(Wdt0 * Hgt0);
  }
  Point3D& Get(int Dex) { return this->Ray.at(Dex); }
  Point3D& Get(int XLoc, int YLoc) {
    int Dex = (YLoc * this->Wdt) + XLoc;
    return this->Ray.at(Dex);
  }
  void Set(int XLoc, int YLoc, Point3D Value) {
    int Dex = (YLoc * this->Wdt) + XLoc;
    this->Ray.at(Dex) = Value;
  }
  int GetDex(int XLoc, int YLoc) { return (YLoc * this->Wdt) + XLoc; }
};

void SaveObj(GridVec &Grid, std::string fname) {
  Point3D pnt;
  std::ofstream file(fname);
  file << "# Terrain Map \n";
  for (int GridDex=0; GridDex<Grid.Ray.size(); GridDex++) {
    pnt = Grid.Get(GridDex);
    // cout << "v 0.123 0.234 0.345 1.0"
    cout << "v " << pnt.X << " " << pnt.Y << " " << pnt.Z << " 1.0 " << "\n";
    file << "v " << pnt.X << " " << pnt.Y << " " << pnt.Z << " 1.0 " << "\n";
  }

  int DragY = 0;
  for (int GridY=1; GridY<Grid.Hgt; GridY++) { // Y is cylinder height.
    int DragX = 0;
    for (int GridX=1; GridX<Grid.Wdt; GridX++) { // X is circumference.
      // "f 1 2 3"
      int Dex0 = Grid.GetDex(DragX, DragY) + 1;
      int Dex1 = Grid.GetDex(DragX, GridY) + 1;
      int Dex2 = Grid.GetDex(GridX, GridY) + 1;
      int Dex3 = Grid.GetDex(GridX, DragY) + 1;
      file << "f " << Dex0 << " " << Dex1 << " " << Dex2 << " " << Dex3 << " " << "\n";
      DragX = GridX;
    }
    DragY = GridY;
  }
  // std::string my_string = "Hello text in file\n";
  // file << my_string;
  file.close();
}

void Test() {
  double TwoPi = M_PI * 2.0;
  double GeoRadius = 1.0;
  int GridWdt = 600, GridHgt = 100;// not really
  GridVec Grid(GridWdt, GridHgt);

  // First create grid array of points.
  double Tilt = TwoPi * 0.01; // 1/100 of a circle
  Tilt = TwoPi * 0.1; // 1/10 of a circle
  // Tilt = TwoPi * 0.9; // 1/2 of a circle
  double GeoCircum = TwoPi * GeoRadius;
  Point3D GeoCenter(0,0,0);

  for (int GridY=0; GridY<GridHgt; GridY++) { // Y is cylinder height.
    double AlongAxis = (  ((double)GridY)/(double)GridHgt  ) - 0.5;   // -0.5 to 0.0 to +0.5
    double SpinCenterY = AlongAxis * ( sin(Tilt) / cos(Tilt) ); // Tangent
    Point3D SpinCenter(0.0, SpinCenterY, AlongAxis);
    for (int GridX=0; GridX<GridWdt; GridX++) { // X is circumference.
      double FractAngle = ((double)GridX)/(double)GridWdt;
      double Angle = FractAngle*TwoPi;
      // Get xy loc on edge of real cylinder.
      double GeoEdgeX = GeoRadius * cos(Angle);
      double GeoEdgeY = GeoRadius * sin(Angle);
      Point3D GeoEdge(GeoEdgeX, GeoEdgeY, AlongAxis);
      double Height = SpinCenter.Distance(GeoEdge);// get xy dist from SpinCenter
      double FlatX = GeoCircum*FractAngle;
      double FlatY = AlongAxis;
      double FlatZ = Height;
      Point3D MapPoint(FlatX, FlatY, FlatZ);
      Grid.Set(GridX, GridY, MapPoint);
    }
  }
  SaveObj(Grid, "Howdy.obj");

  return;

  for (double AlongAxis=-0.5; AlongAxis<0.5; AlongAxis+=0.01) { // -1 to 0 to +1
    double SpinCenterY = AlongAxis * ( sin(Tilt) / cos(Tilt) ); // Tangent
    Point3D SpinCenter(0.0, SpinCenterY, AlongAxis);
    for (double FractAngle=0.0; FractAngle<1.0; FractAngle+=0.01) { // 0 to almost 1.0,    for all FractAngles GeoCenter
      double Angle = FractAngle*TwoPi;
      // Get xy loc on edge of real cylinder.
      double GeoEdgeX = GeoRadius * cos(Angle);
      double GeoEdgeY = GeoRadius * sin(Angle);
      Point3D GeoEdge(GeoEdgeX, GeoEdgeY, AlongAxis);
      double Height = SpinCenter.Distance(GeoEdge);// get xy dist from SpinCenter
      double FlatX = GeoCircum*FractAngle;
      double FlatY = AlongAxis;
      double FlatZ = Height;
      Point3D MapPoint(FlatX, FlatY, FlatZ);
      // Grid.Set(GridX, GridY, MapPoint);
/*
    save (GeoCircum*FractAngle), AlongAxis, dist // x y z
    // or
    // save (FractAngle*TwoPi*GeoRadius), AlongAxis, dist // x y z
*/
    }
  }
}

int main() {
    cout << "Hello world!" << endl;
    Test();
    return 0;
}

/*
  // Barrel Terrain math
  public class Point3D {
    public double X, Y, Z;
    public Point3D(double X0, double Y0, double Z0) {
    this.X=X0; this.Y=Y0; this.Z=Z0;
    }
    public double Magnitude() {
    return Math.sqrt( (this.X*this.X) + (this.Y*this.Y) + (this.Z*this.Z) );
    }
  }

  double Tilt = TwoPi * 0.01; // 1/100 of a circle
  double GeoCircum = TwoPi * GeoRadius;
  Point3D GeoCenter = new Point3D(0,0,0);

  for (double AlongAxis=-1.0; AlongAxis<1.0; AlongAxis+=0.01) { // -1 to 0 to +1
  // for all AlongAxiss along geoaxis { // -1 to 0 to +1
   double SpinCenterY = AlongAxis * ( Math.sin(Tilt) / Math.cos(Tilt) ) // Tangent
   Point3D SpinCenter = new Point3D(0, SpinCenterY, AlongAxis);
   for all FractAngles GeoCenter {
    // get xy loc on edge of real cylinder
    get xy dist from SpinCenter
    save (GeoCircum*FractAngle), AlongAxis, dist // x y z
    // or
    // save (FractAngle*TwoPi*GeoRadius), AlongAxis, dist // x y z
   }
  }
*/
